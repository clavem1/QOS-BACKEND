# Dockerfile for qos main service

FROM node:10.6.0

MAINTAINER Mikiyas Amdu <mikias.amdu@gmail.com>

ADD . /home/qos-service

WORKDIR /home/qos-service

RUN npm install

EXPOSE 7600

RUN npm run build

ENTRYPOINT ["node", "dist/bin/www.js"]
