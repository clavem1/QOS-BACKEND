const featureModel = require('../models/feature-model')

/**
 * Feature model store.
 *
 * gets the logger injected.
 * gets redis Client injected
 */
export default function createFeatureStore(logger, client) {
  let model = featureModel

  let collectionName = model.collection.name

  return {
    async find() {
      logger.debug(`Finding ${collectionName}`)
      let value = await client.get(collectionName)
      if (!value) {
        value = await model.find({})
        if (!value) {
          return null
        }
        await client.set(collectionName, JSON.stringify(value))
        return value
      }
      return JSON.parse(value)
    },

    async paginate(query, options) {
      logger.debug(`Finding and paginating ${collectionName}`)
    },

    async get(id) {
      logger.debug(`Getting ${collectionName} with id ${id}`)

      let value = await client.get(`${collectionName}_${id}`)
      if (!value) {
        value = await model.findOne({
          _id: id.toString()
        })
        if (!value) {
          return null
        }
        await client.set(`${collectionName}_${id}`, JSON.stringify(value))
        return value
      }

      return JSON.parse(value)
    },

    async create(data) {
      const result = await model.create(data)
      logger.debug(`Created new ${collectionName}`, result)
      return result
    },

    async update(id, data) {
      const result = model.find({
        _id: id.toString()
      })
      Object.assign(result, data)
      logger.debug(`Updated ${collectionName} ${id}`, result)
      return result
    },

    async remove(id) {
      model.delete(x => x._id === id.toString())
      logger.debug(`Removed ${collectionName} ${id}`)
    }
  }
}
