const fullInfoModel = require('../models/fullinfo-model')

/**
 * createFullinfoStore model store.
 *
 * gets the logger injected.
 * gets redis clients injected.
 */
export default function createFullinfoStore(logger, client) {
  let model = fullInfoModel

  let collectionName = model.collection.name

  return {
    async find(params, hash) {
      logger.debug(`Finding ${collectionName}`)

      let value = await client.get(hash)

      if (!value) {
        let result = await model.aggregate([
          {
            $match: params
          },
          {
            $group: {
              _id: '$cellId',
              operateur: {
                $first: '$operateur'
              },
              address: {
                $first: '$address'
              },
              network_type: {
                $first: '$type_reseau'
              },
              latitude: {
                $first: '$latitude'
              },
              longitude: {
                $first: '$longitude'
              },
              average: {
                $avg: '$signal_strength'
              }
            }
          }
        ])

        await client.set(hash, JSON.stringify(result))
        return result
      }

      return JSON.parse(value)
    },

    async average(query) {
      let redisKeyName = collectionName + '_average'

      if (query.match.operateur)
        redisKeyName = redisKeyName + '_' + query.match.operateur

      if (query.match.cellId)
        redisKeyName = redisKeyName + '_' + query.match.cellId[0]

      let value = await client.get(redisKeyName)
      if (!value) {
        value = await model.aggregate([
          {
            $match: {
              $and: [query.match]
            }
          },
          {
            $group: {
              _id: null,
              average: {
                $avg: '$signal_strength'
              }
            }
          }
        ])
        await client.set(redisKeyName, JSON.stringify(value))

        return value
      }
      return JSON.parse(value)
    },
    /**
     * Paginate the given request
     * @param {*} query
     * @param {*} options
     */
    async paginate(query, options) {
      logger.debug(`Finding and paginating ${collectionName}`)
      let redisKeyName =
        collectionName + '_' + options.page + '_' + options.limit
      let match = {}

      if (query.operateur) {
        redisKeyName = redisKeyName + '_operateur_' + query.operateur
        match['operateur'] = query.operateur
      }

      if (query.cellId) {
        redisKeyName = redisKeyName + '_commune_' + query.cellId[0]
        match['cellId'] = query.cellId
      }

      if (query.date) {
      }

      let value = await client.get(redisKeyName)
      const average = await this.average({
        match
      })

      // if (!value) {
      value = await model.paginate(query, options)
      if (!value) {
        return null
      }
      await client.set(redisKeyName, JSON.stringify(value))
      return {
        ...value,
        ...average[0]
      }
      // }
      // return {
      //   ...JSON.parse(value),
      //   ...average[0]
      // }
    },
    /**
     * GET
     * @param {*} id
     */
    async get(id) {
      logger.debug(`Getting ${collectionName} with id ${id}`)
      const found = model.find({
        _id: id.toString()
      })
      if (!found) {
        return null
      }
      return found
    },
    /**
     *
     * @param {*} data
     */
    async create(data) {
      let result = await model.create(data)
      logger.debug(`Created new ${collectionName}`, result)
      return result
    },
    /**
     *
     * @param {*} id
     * @param {*} data
     */
    async update(id, data) {
      const result = model.find({
        _id: id.toString()
      })
      Object.assign(result, data)
      logger.debug(`Updated ${collectionName} ${id}`, result)
      return result
    },
    /**
     *
     */
    async remove(id) {
      model.delete(x => x._id === id.toString())
      logger.debug(`Removed ${collectionName} ${id}`)
    }
  }
}
