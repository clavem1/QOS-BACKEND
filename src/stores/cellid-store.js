const cellidModel = require('../models/cellid-model')

/**
 * Cellid model store.
 *
 * gets the logger injected.
 * gets redis Client
 */
export default function createCellidStore(logger, client) {
  let model = cellidModel

  let collectionName = model.collection.name

  return {
    async find(params) {
      console.log(params, 'params')
      logger.debug(`Finding ${collectionName}`)
      return model.find(params)
    },

    async paginate(query, options) {
      logger.debug(`Finding and paginating ${collectionName}`)
      return model.paginate(query, options)
    },
    async get(id) {
      logger.debug(`Getting ${collectionName} with id ${id}`)

      let value = await client.get(`${collectionName}_${id}`)

      if (!value) {
        value = await model.findOne({
          _id: id.toString()
        })
        if (!value) {
          return null
        }
        await client.set(`${collectionName}_${id}`, JSON.stringify(value))
        return { ...JSON.parse(JSON.stringify(value)) }
      }

      return { ...JSON.parse(value) }
    },

    async create(data) {
      let result = await model.create(data)
      logger.debug(`Created new ${collectionName}`, result)
      return result
    },

    async update(id, data) {
      const result = model.find({ _id: id.toString() })
      Object.assign(result, data)
      logger.debug(`Updated ${collectionName} ${id}`, result)
      return result
    },

    async remove(id) {
      model.delete(x => x._id === id.toString())
      logger.debug(`Removed ${collectionName} ${id}`)
    }
  }
}
