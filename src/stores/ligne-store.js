const ligneModel = require('../models/ligne-model')

/**
 * ligne model store.
 *
 * gets the logger injected.
 * gets redis client injected.
 */
export default function createLigneStore(logger, client) {
  let model = ligneModel

  let collectionName = model.collection.name

  return {
    async find() {
      logger.debug(`Finding ${collectionName}`)
      return model.find({})
    },

    async get(id) {
      logger.debug(`Getting ${collectionName} with id ${id}`)
      const found = model.find({
        _id: id.toString()
      })
      if (!found) {
        return null
      }
      return found
    }
  }
}
