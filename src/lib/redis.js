import { env } from './env'
const asyncRedis = require('async-redis')

export const client = asyncRedis.createClient(env.REDIS_PORT, env.REDIS_HOST)
