import { createContainer, Lifetime, ResolutionMode } from 'awilix'
import { logger } from './logger'
import { client } from './redis'
/**
 * Using Awilix, the following files and folders (glob patterns)
 * will be loaded.
 */
const modulesToLoad = [
  // Services should be scoped to the request.
  // This means that each request gets a separate instance
  // of a service.
  ['services/*.js', Lifetime.SCOPED],
  // Stores will be singleton (1 instance per process).
  ['stores/*.js', Lifetime.SINGLETON],
  // Models will be singleton (1 instance per process).
  ['models/*.js', Lifetime.TRANSIENT]
]

/**
 * Configures a new container.
 *
 * @return {Object} The container.
 */
export function configureContainer() {
  const opts = {
    // Classic means Awilix will look at function parameter
    // names rather than passing a Proxy.
    resolutionMode: ResolutionMode.CLASSIC
  }
  return createContainer(opts)
    .loadModules(modulesToLoad, {
      // `modulesToLoad` paths should be relative
      // to this file's parent directory.
      cwd: `${__dirname}/..`,
      // Example: registers `services/todo-service.js` as `todoService`
      formatName: 'camelCase'
    })
    .registerValue({
      // Our logger is already constructed,
      // so provide it as-is to anyone who wants it.
      logger,
      client
    })
}
