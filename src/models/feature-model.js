import mongoose from 'mongoose'
import mongoosePaginate from 'mongoose-paginate'

// var Schema = mongoose.Schema
// var Schema =  mongoose.Schema;
/**
 * FeatureSchema
 */
const FeatureSchema = (mongoose.Schema = {
  type: { type: String },
  properties: { type: Object },
  geometry: { type: Object },
  cellid: { type: Array }
})

/**
 * Model Attributes to expose
 */
mongoose.whitelist = {
  type: 1,
  properties: 1,
  geometry: 1,
  cellid: 1
}

mongoose.plugin(mongoosePaginate)

module.exports = mongoose.model('Features', FeatureSchema)
