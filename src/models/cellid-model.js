import mongoose from 'mongoose'
import mongoosePaginate from 'mongoose-paginate'
/**
 * CellidSchema
 */
const CellidSchema = (mongoose.Schema = {})

mongoose.plugin(mongoosePaginate)

module.exports = mongoose.model(
  'Commune_with_cellid',
  CellidSchema,
  'commune_with_cellid'
)
