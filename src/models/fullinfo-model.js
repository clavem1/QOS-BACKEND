import mongoose from 'mongoose'

import mongoosePaginate from 'mongoose-paginate'

const FullInfoSchema = mongoose.model(
  'FullInfo',
  { name: String },
  'prepairgeo_full_info'
)

/**
 * UserSchema
 */
// const FullInfoSchema =  mongoose.Schema({
// })

// console.log(FullInfoSchema.schema._indexes)
// FullInfoSchema.schema.index({operateur: 1},  {name: 'operateur_index'});

mongoose.plugin(mongoosePaginate)

// module.exports = mongoose.model(
//   'FullInfo',
//   FullInfoSchema,
//   'prepairgeo_full_info'
// )

// module.exports = mongoose.model('FullInfo', FullInfoSchema, 'prepairgeo_full_info')

module.exports = FullInfoSchema
