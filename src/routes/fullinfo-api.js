import { createController } from 'awilix-koa'

import passport from 'koa-passport'

import '../lib/passport'

// This is our API controller.
// All it does is map HTTP calls to service calls.
// This way our services could be used in any type of app, not
// just over HTTP.
const api = fullinfoService => ({
  findFullinfo: async ctx => ctx.ok(await fullinfoService.find(ctx.query))
})

// Maps routes to method calls on the `api` controller.
// See the `awilix-router-core` docs for info:
// https://github.com/jeffijoe/awilix-router-core
export default createController(api)
  .prefix('/geo-infos')
  .before([passport.authenticate('jwt', { session: false })])
  .get('', 'findFullinfo')
