import { createController } from 'awilix-koa'

import passport from 'koa-passport'

import '../lib/passport'

// This is our API controller.
// All it does is map HTTP calls to service calls.
// This way our services could be used in any type of app, not
// just over HTTP.
const api = featureService => ({
  findFeature: async ctx => ctx.ok(await featureService.find(ctx.query)),
  getFeature: async ctx => ctx.ok(await featureService.get(ctx.params.id)),
  createFeature: async ctx => {
    return ctx.created(await featureService.create(ctx.request.body))
  },
  updateFeature: async ctx =>
    ctx.ok(await featureService.update(ctx.params.id, ctx.request.body)),
  removeFeature: async ctx =>
    ctx.noContent(await featureService.remove(ctx.params.id))
})

// Maps routes to method calls on the `api` controller.
// See the `awilix-router-core` docs for info:
// https://github.com/jeffijoe/awilix-router-core
export default createController(api)
  .prefix('/features')
  .before([passport.authenticate('jwt', { session: false })])
  .get('', 'findFeature')
  .get('/:id', 'getFeature')
  .post('', 'createFeature')
  .patch('/:id', 'updateFeature')
  .delete('/:id', 'removeFeature')
