import { createController } from 'awilix-koa'

import passport from 'koa-passport'

import '../lib/passport'

// This is our API controller.
// All it does is map HTTP calls to service calls.
// This way our services could be used in any type of app, not
// just over HTTP.
const api = cellidService => ({
  findCellid: async ctx => ctx.ok(await cellidService.find(ctx.query)),
  getCellid: async ctx => ctx.ok(await cellidService.get(ctx.params.id))
})

// Maps routes to method calls on the `api` controller.
// See the `awilix-router-core` docs for info:
// https://github.com/jeffijoe/awilix-router-core
export default createController(api)
  .prefix('/cell-ids')
  .before([passport.authenticate('jwt', { session: false })])
  .get('', 'findCellid')
  .get('/:id', 'getCellid')
  .post('', 'createCellid')
  .patch('/:id', 'updateCellid')
  .delete('/:id', 'removeCellid')
