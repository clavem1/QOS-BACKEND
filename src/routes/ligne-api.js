import { createController } from 'awilix-koa'

import passport from 'koa-passport'

import '../lib/passport'

// This is our API controller.
// All it does is map HTTP calls to service calls.
// This way our services could be used in any type of app, not
// just over HTTP.
const api = ligneService => ({
  findLigne: async ctx => ctx.ok(await ligneService.find(ctx.query)),
  getLigne: async ctx => ctx.ok(await ligneService.get(ctx.params.id))
})

// Maps routes to method calls on the `api` controller.
// See the `awilix-router-core` docs for info:
// https://github.com/jeffijoe/awilix-router-core
export default createController(api)
  .prefix('/lignes')
  .before([passport.authenticate('jwt', { session: false })])
  .get('', 'findLigne')
  .get('/:id', 'getLigne')
