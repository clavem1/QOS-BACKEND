/**
 * Commune Service.
 */
export default class CommuneService {
  constructor(featureStore) {
    this.store = featureStore
  }

  async find(params) {
    const query = {}
    const options = {
      select: { 'properties.Name': 1 },
      page: parseInt(params.page) || 1,
      sort: params.sort || {},
      lean: false,
      limit: parseInt(params.limits) || 10
    }
    const features = await this.store.paginate(query, options)
    if (!features) {
      throw new Error('There was an error retrieving features.')
    } else {
      return features.docs.map(value => {
        return value.properties.Name
      })
    }
  }
}
