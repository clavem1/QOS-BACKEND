import { BadRequest } from 'fejl'

var crypto = require('crypto')

/**
 * Fullinfo Service.
 */
export default class FullinfoService {
  constructor(fullinfoStore, featureStore, cellidStore) {
    this.store = fullinfoStore
    this.feature = featureStore
    this.commune = cellidStore
  }

  async find(params) {
    BadRequest.assert(params, 'No payload given')
    BadRequest.assert(params.start_date, 'Start date is required')
    BadRequest.assert(params.end_date, 'End date is required')
    const hash = {
      start_date: params.start_date,
      end_date: params.end_date
    }
    const query = {}
    query['date'] = {
      $gt: new Date(params.start_date),
      $lt: new Date(params.end_date)
    }

    if (params.operator) {
      query['operateur'] = parseInt(params.operator)
      hash['operator'] = parseInt(params.operator)
    }

    if (params.commune_id) {
      hash['commune_id'] = params.commune_id
      let commune = await this.commune.get(params.commune_id)
      if (!commune) {
        throw new Error('There was an error retrieving commune info.')
      }
      query['cellId'] = {
        $in: Object.assign({}, commune)['cellId']
      }
    }
    const data = await this.store.find(
      query,
      crypto
        .createHash('md5')
        .update(JSON.stringify(hash))
        .digest('hex')
    )

    // calculate average of the operators and total
    const provider = {
      tigo_average: 0,
      tigo_count: 0,
      expresso_average: 0,
      expresso_count: 0,
      total_average: 0,
      total: 0
    }
    data.forEach(element => {
      if (element['operateur'] === 2) {
        provider['tigo_average'] = provider['tigo_average'] + element['average']
        provider['tigo_count']++
      }

      if (element['operateur'] === 3) {
        provider['expresso_average'] =
          provider['expresso_average'] + element['average']
        provider['expresso_count']++
      }

      provider['total_average'] = provider['total_average'] + element['average']
      provider['total']++
    })

    if (!data) {
      throw new Error('There was an error retrieving geo info.')
    } else {
      return {
        average: provider['total_average'] / provider['total'],
        tigo_average: provider['tigo_average'] / provider['tigo_count'],
        expresso_average:
          provider['expresso_average'] / provider['expresso_count'],
        docs: data
      }
    }
  }
}
