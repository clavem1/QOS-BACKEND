import { throws } from 'smid'
import CellidService from '../cellid-service'

// This test only verify invariants, not interaction with dependencies.
// That is tested with integration tests.
describe('CellidService', () => {
  describe('find', () => {
    it('can find cellids', async () => {
      const { service, tests } = setup()
      expect(await service.find()).toEqual(tests)
    })
  })

  describe('get', () => {
    it('throws when not found', async () => {
      const { service, tests } = setup()
      expect((await throws(service.get('nonexistent'))).message).toMatch(
        /not found/
      )
      expect(await service.get('1')).toEqual(tests[0])
    })
  })
})
function setup() {
  const tests = [
    { id: '1', type: 'Feature', properties: {}, geometry: {}, cellid: {} },
    { id: '2', type: 'Feature', properties: {}, geometry: {}, cellid: {} }
  ]
  // Mock store
  const store = {
    find: jest.fn(async () => [...tests]),
    get: jest.fn(async id => tests.find(x => x.id === id))
  }
  return { service: new CellidService(store), store, tests }
}
