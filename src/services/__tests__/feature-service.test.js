import { throws } from 'smid'
import FeatureService from '../feature-service'

// This test only verify invariants, not interaction with dependencies.
// That is tested with integration tests.
describe('FeatureService', () => {
  describe('find', () => {
    it('can find features', async () => {
      const { service, tests } = setup()
      expect(await service.find()).toEqual(tests)
    })
  })

  describe('get', () => {
    it('throws when not found', async () => {
      const { service, tests } = setup()
      expect((await throws(service.get('nonexistent'))).message).toMatch(
        /not found/
      )

      expect(await service.get('1')).toEqual(tests[0])
    })
  })

  describe('create', () => {
    it('throws when no payload is given', async () => {
      const { service } = setup()
      const err = await throws(service.create())
      expect(err.message).toMatch(/payload/)
    })

    it('throws when type is invalid', async () => {
      const { service } = setup()
      expect((await throws(service.create())).message).toMatch(/payload/)
      expect((await throws(service.create({}))).message).toMatch(/type/)
    })

    it('removes unknown props', async () => {
      const { service } = setup()
      expect(
        await service.create({
          type: 'test',
          properties: {},
          geometry: {},
          cellid: {},
          removeme: 'please'
        })
      ).toEqual({
        cellid: {},
        geometry: {},
        properties: {},
        type: 'test'
      })
    })
  })

  describe('update', () => {
    it('throws when feature does not exist', async () => {
      const { service } = setup()
      expect((await throws(service.update('nonexisting', {}))).message).toMatch(
        /nonexisting/
      )

      await service.update('1', { type: 'hello' })
    })
  })

  describe('remove', () => {
    it('throws when test does not exist', async () => {
      const { service } = setup()
      expect((await throws(service.remove('nonexisting'))).message).toMatch(
        /nonexisting/
      )
      await service.remove('1')
    })
  })
})

function setup() {
  const tests = [
    { id: '1', type: 'Feature', properties: {}, geometry: {}, cellid: {} },
    { id: '2', type: 'Feature', properties: {}, geometry: {}, cellid: {} }
  ]
  // Mock store
  const store = {
    find: jest.fn(async () => [...tests]),
    get: jest.fn(async id => tests.find(x => x.id === id)),
    create: jest.fn(async data => ({ ...data })),
    update: jest.fn(async (id, data) => ({ ...data })),
    remove: jest.fn(async id => undefined)
  }
  return { service: new FeatureService(store), store, tests }
}
