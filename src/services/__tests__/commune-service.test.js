import CommuneService from '../commune-service'

// This test only verify invariants, not interaction with dependencies.
// That is tested with integration tests.
describe('CommuneService', () => {
  describe('find', () => {
    it('can find commune', async () => {
      const { service, tests } = setup()
      expect(await service.find()).toEqual(
        tests.map(value => value._doc.properties.Name)
      )
    })

    it('can return not found', async () => {
      const { service } = setup(true)
      expect(await service.find({ hah: 'lala' })).toEqual([])
    })
  })
})

function setup(empty) {
  let tests = []
  if (!empty) {
    tests = [
      {
        _doc: {
          id: '1',
          type: 'Feature',
          properties: {
            Name: 'sample'
          },
          geometry: {},
          cellid: {}
        }
      },
      {
        _doc: {
          id: '1',
          type: 'Feature2',
          properties: {
            Name: 'sample'
          },
          geometry: {},
          cellid: {}
        }
      }
    ]
  }
  // Mock store
  const store = {
    find: jest.fn(async () => [...tests])
  }
  return {
    service: new CommuneService(store),
    store,
    tests
  }
}
