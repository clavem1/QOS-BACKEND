import { NotFound, BadRequest } from 'fejl'
import { pick } from 'lodash'

// Prefab assert function.
const assertId = BadRequest.makeAssert('No id given')

// Prevent overposting.
const pickProps = data =>
  pick(data, ['type', 'properties', 'geometry', 'cellid'])

/**
 * Feature Service.
 */
export default class FeatureService {
  constructor(featureStore) {
    this.store = featureStore
  }

  async find(params) {
    console.log('here')
    const features = await this.store.find(params)

    if (!features) {
      throw new Error('There was an error retrieving features.')
    } else {
      return features
    }
  }
  async get(id) {
    assertId(id)
    // If `Store.get()` returns a falsy value, we throw a
    // NotFound error with the specified message.
    return this.store
      .get(id)
      .then(NotFound.makeAssert(`Feature with id "${id}" not found`))
  }

  async create(data) {
    BadRequest.assert(data, 'No feature payload given')
    BadRequest.assert(data.type, 'type is required')
    BadRequest.assert(data.properties, 'properties is required')
    BadRequest.assert(data.geometry, 'geometry is required')
    BadRequest.assert(data.cellid, 'geometry is required')
    BadRequest.assert(
      typeof data.properties === 'object',
      'properties has to be an object'
    )
    return this.store.create(pickProps(data))
  }

  async update(id, data) {
    assertId(id)
    BadRequest.assert(data, 'No feature payload given')

    // Make sure the model exists by calling `get`.
    await this.get(id)

    // Prevent overposting.
    const picked = pickProps(data)
    return this.store.update(id, picked)
  }

  async remove(id) {
    // Make sure the model exists by calling `get`.
    await this.get(id)
    return this.store.remove(id)
  }
}
