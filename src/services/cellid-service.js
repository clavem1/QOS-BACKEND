import { NotFound, BadRequest } from 'fejl'

// Prefab assert function.
const assertId = BadRequest.makeAssert('No id given')

/**
 * Cellid Service.
 */
export default class CellidService {
  constructor(cellidStore) {
    this.store = cellidStore
  }

  async find(params) {
    const query = {}

    if (params.name) {
      query['NOM'] = params.name
    }

    const options = {
      select: '',
      page: parseInt(params.page) || 1,
      sort: params.sort || { date: -1 },
      lean: false,
      limit: parseInt(params.limits) || 50
    }

    const features = await this.store.paginate(query, options)
    if (!features) {
      throw new Error('There was an error retrieving features.')
    } else {
      return features
    }
  }
  async get(id) {
    assertId(id)
    // If `Store.get()` returns a falsy value, we throw a
    // NotFound error with the specified message.
    return this.store
      .get(id)
      .then(NotFound.makeAssert(`Feature with id "${id}" not found`))
  }
}
