import { NotFound, BadRequest } from 'fejl'

// Prefab assert function.
const assertId = BadRequest.makeAssert('No id given')

/**
 * Ligne Service.
 */
export default class LigneService {
  constructor(ligneStore) {
    this.store = ligneStore
  }

  async find(params) {
    const features = await this.store.find(params)
    if (!features) {
      throw new Error('There was an error retrieving Ligne.')
    } else {
      return features
    }
  }
  async get(id) {
    assertId(id)
    // If `Store.get()` returns a falsy value, we throw a
    // NotFound error with the specified message.
    return this.store
      .get(id)
      .then(NotFound.makeAssert(`Ligne with id "${id}" not found`))
  }
}
